<?php

namespace Tests\Unit\Category;

use Webmagic\Blog\Categories\CategoryRepo as BaseRepo;

class CategoryRepo extends BaseRepo
{
    public function setEntity($entity_name)
    {
        $this->entity = $entity_name;
    }
}