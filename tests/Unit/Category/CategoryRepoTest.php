<?php

namespace Tests\Unit\Category;

use Illuminate\Database\Eloquent\Model as Model;
use Tests\TestCase;
use Webmagic\Blog\Categories\Category;
use Webmagic\Blog\Posts\Post;
use IvanLemeshev\Laravel5CyrillicSlug\Slug;


class CategoryRepoTest extends TestCase
{
    /**
     * CategoryRepo
     * @var
     */
    protected $repo;

    public function setUp()
    {
        parent::setUp();

        $this->repo = new CategoryRepo();
        $this->repo->setEntity(Category::class);
    }

    /**
     * Test function for creating a category
     */
    public function testCreate()
    {
        $slug = new Slug();

        $category_name = 'category name';
        $category_slug = $slug->make($category_name, '-');

        $new_category = $this->repo->create([
            'name' => $category_name,
            'slug' => ''
        ]);

        $category = $this->repo->getByID($new_category->id);

        $this->assertTrue(is_subclass_of($category, Model::class));
        $this->assertEquals($category_name, $category->name);
        $this->assertEquals($category_slug, $category->slug);
    }

    /**
     * Test function for updating a category
     */
    public function testUpdate()
    {
        $slug = new Slug();
        //create and update parent category
        $category_new_name = 'new category name';
        $category_new_slug = $slug->make($category_new_name, '-');
        $category =  factory(Category::class)->create();

        $this->repo->update($category->id,
            ['name' => $category_new_name,
            'slug' => '']);

        $updated_category = Category::find($category->id);

        //Check if category is equal to new entity
        $this->assertEquals($updated_category->name,  $category_new_name);
        $this->assertEquals($updated_category->slug, $category_new_slug);

    }

    /**
     *  Test function for getting category by id
     */
    public function testGetByID()
    {
        $category_name = 'category name';

        $new_category = factory(Category::class)->create(['name' => $category_name]);

        factory(blog::class, 2)->create([
            'category_id' => $new_category->id
        ]);

        //With posts
        $category = $this->repo->getByID($new_category->id);

        $this->assertTrue(is_subclass_of($category, Model::class));
        $this->assertEquals($category_name, $category->name);

        $this->assertArrayHasKey('posts', $category);
        $this->assertEquals(2, count($category->posts));

    }


    /**
     *  Test function for getting all categories
     */
    public function testGetAll()
    {
        $categories_collection = $this->repo->getAll();

        //Check instance in no one category
        $this->assertTrue($categories_collection instanceof \Illuminate\Database\Eloquent\Collection);
        $this->assertCount(0, $categories_collection);

        //Creating categories
        $category = factory(Category::class)->create();

        //Creating post
        factory(blog::class)->create([
            'category_id' => $category->id
        ]);

        //With posts
        $categories_collection = $this->repo->getAll();

        $this->assertTrue($categories_collection instanceof \Illuminate\Database\Eloquent\Collection);
        $this->assertArrayHasKey('posts', $categories_collection->first()->toArray());
        $this->assertEquals(1, count($categories_collection->first()['posts']));
    }


    /**
     *  Test function for getting all active categories
     */
    public function testGetAllActive()
    {
        $category = factory(Category::class)->create(['active' => false]);
        factory(blog::class)->create([
            'category_id' => $category->id,
            'slug' => ''
        ]);

        //Check there is no one active category
        $categories_collection = $this->repo->getAllActive();

        $this->assertTrue($categories_collection instanceof \Illuminate\Database\Eloquent\Collection);
        $this->assertCount(0, $categories_collection);


        $this->repo->update($category->id, [
            'name' => 'updated_category',
            'slug' => $category->name,
            'active' => true,
        ]);
        $updated_category = Category::find($category->id);

        //With posts
        $categories_collection = $this->repo->getAllActive();

        $this->assertTrue($categories_collection instanceof \Illuminate\Database\Eloquent\Collection);
        $this->assertEquals($updated_category->name, $categories_collection->first()->name);
        $this->assertArrayHasKey('posts', $categories_collection->first()->toArray());
        $this->assertEquals(1, count($categories_collection->first()['posts']));
    }

}