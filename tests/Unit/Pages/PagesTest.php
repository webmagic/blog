<?php

namespace tests\Unit\Pages;


use Tests\TestCase;
use Webmagic\Users\Models\User;
use Webmagic\Blog\Posts\Post;
use Webmagic\Blog\Categories\Category;


class PagesTest extends TestCase
{

    /**
     * Post
     */
    public function testPostIndex()
    {

        $user = new User();
        $response = $this->actingAs($user)->get(route('blog::index'));
        $response->assertStatus(200);
    }



    /**
     * Page for post creating
     */
    public function testPostCreate()
    {

        $user = new User();
        $response = $this->actingAs($user)->get(route('blog::create'));
        $response->assertStatus(200);
    }

    /**
     * Page for post updating
     */
    public function testPostUpdate()
    {
        $user = new User();
        $post = factory(blog::class)->create();

        $response = $this->actingAs($user)->get(route('blog::edit', $post->id));
        $response->assertStatus(200);
    }

    /**
     *  Page for category list
     */
    public function testCategoryIndex()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('category::index'));
        $response->assertStatus(200);
    }

    /**
     *  Page for category creating
     */
    public function testCategoryCreate()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('category::create'));
        $response->assertStatus(200);
    }

    /**
     *  Page for category updating
     */
    public function testCategoryUpdate()
    {
        $user = new User();
        $category = factory(Category::class)->create();

        $response = $this->actingAs($user)->get(route('category::edit', $category->id));
        $response->assertStatus(200);
    }
}
