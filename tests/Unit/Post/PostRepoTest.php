<?php

namespace Tests\Unit\Post;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;
use Webmagic\Blog\Categories\Category;
use Webmagic\Blog\Posts\Post;


use IvanLemeshev\Laravel5CyrillicSlug\Slug;


class PostRepoTest extends TestCase
{
    /**
     * postRepo
     * @var
     */
    protected $repo;

    public function setUp()
    {
        parent::setUp();
        $this->repo = new PostRepo();
        $this->repo->setEntity(blog::class);
    }


    /**
     * Test function for creating post
     */
    public function testCreate()
    {
        $slug = new Slug();
        //create new category
        $category = factory(Category::class)
            ->create([
                'name' => 'new_category',
                'slug' => '',
            ]);
        $post_name = 'post name';
        $post_slug = $slug->make($post_name, '-');
        $post_category = $category->id;

        $new_post = $this->repo->create([
            'name' => $post_name,
            'slug' => '',
            'category_id' => $category->id
        ]);

        $post = $this->repo->getByID($new_post->id);

        $this->assertTrue(is_subclass_of($post, Model::class));
        $this->assertEquals($post_name, $post->name);
        $this->assertEquals($post_slug, $post->slug);
        $this->assertEquals($post_category, $post->category_id);
    }


    /**
     * Test function for updating a category
     */
    public function testUpdate()
    {
        $slug = new Slug();

        $post_name = 'post name';
        $post_slug = $slug->make($post_name, '-');

        //Create new category for post
        $category =  factory(Category::class)->create();
        //create  new post
        $post =  factory(blog::class)->create();

        //update post
        $this->repo->update($post->id, [
            'name' => $post_name,
            'category_id' => $category->id,
            'slug' => '',
        ]);

        $updated_post = blog::find($post->id);

        //Check if post is equal to new entity
        $this->assertEquals($updated_post->name,  $post_name);
        $this->assertEquals($updated_post->slug, $post_slug);
    }


    /**
     * Test posts filtered by slug
     */
    public function testGetBySlug()
    {
        $slug = new Slug();
        $post_name = 'post name';
        $post_slug = $slug->make($post_name, '-');

        $first_post = factory(blog::class)->create([
            'name' => $post_name,
            'slug' => $post_slug,

        ]);

        $post = $this->repo->getBySlug($post_slug);

        $this->assertTrue(is_subclass_of($post, \Illuminate\Database\Eloquent\Model::class));
        $this->assertEquals($post_name, $first_post->name);
    }


    /**
     * Test posts filtered by ID
     */
    public function testGetPostById()
    {
        $post_name = 'post name';

        $first_post = factory(blog::class)->create([
            'name' => $post_name,
        ]);
        $second_post = factory(blog::class)->create();

        $post = $this->repo->getByID($first_post->id);

        $this->assertTrue(is_subclass_of($post, \Illuminate\Database\Eloquent\Model::class));
        $this->assertEquals($post_name, $first_post->name);
    }

    /**
     * Test function for getting all posts
     */
    public function testGetAll()
    {
        $posts_per_page = 2;

        //Without pagination check instance of empty collection
        $posts_collection = $this->repo->getAll();

        $this->assertTrue($posts_collection instanceof Collection);
        $this->assertCount(0, $posts_collection);

        //With pagination check instance of empty collection
        $posts_collection = $this->repo->getAll($posts_per_page);

        $this->assertTrue($posts_collection instanceof LengthAwarePaginator);
        $this->assertCount(0, $posts_collection);

        factory(blog::class, 3)->create();

        //Without pagination
        $posts_collection = $this->repo->getAll();

        $this->assertTrue($posts_collection instanceof Collection);
        $this->assertCount(3, $posts_collection);

        //With pagination
        $posts_collection = $this->repo->getAll($posts_per_page);

        $this->assertTrue($posts_collection instanceof LengthAwarePaginator);
        $this->assertCount(2, $posts_collection->items());
        $this->assertEquals(3, $posts_collection->total());
        $this->assertEquals($posts_per_page, $posts_collection->perPage());
    }


    /**
     * Test posts filtered by category ID
     */
    public function testGetPostsByCategoryId()
    {
        $posts_per_page = 2;
        $category = factory(Category::class)->create();

        //Check instance of empty collection
        $posts_collection = $this->repo->getByCategoryId($category->id);

        $this->assertTrue($posts_collection instanceof Collection);
        $this->assertCount(0, $posts_collection);

        //Creating posts
        $first_post = factory(blog::class)->create();
        $second_post = factory(blog::class)->create([
            'category_id' => $category->id
        ]);

        //Without posts
        $posts_collection = $this->repo->getByCategoryId($category->id);

        $this->assertTrue($posts_collection instanceof Collection);
        $this->assertCount(1, $posts_collection);
        $this->assertEquals($second_post->id, $posts_collection->first()->id);

        //With posts
        $posts_collection = $this->repo->getByCategoryId($category->id, $posts_per_page);

        $this->assertTrue($posts_collection instanceof LengthAwarePaginator);
        $this->assertCount(1, $posts_collection->items());
        $this->assertEquals($posts_per_page, $posts_collection->perPage());
    }


    /**
     * Test posts filtered by part of name or full name
     */
    public function testSearchByName()
    {
        $first_post = factory(blog::class)->create([
            'name' => 'first post'

        ]);

        $second_post = factory(blog::class)->create([
            'name' => 'second post'

        ]);

        $third_post = factory(blog::class)->create([]);

        $posts_per_page = 2;

        //Search by not exist name
        $result = $this->repo->searchByName('test');
        $this->assertTrue($result instanceof Collection);
        $this->assertCount(0, $result);

        //Search by full name
        $result = $this->repo->searchByName($first_post->name);

        $this->assertTrue($result instanceof Collection);
        $this->assertCount(1, $result);
        $this->assertEquals($first_post->name, $result->first()->name);

        //Search by part of name without pagination
        $result = $this->repo->searchByName('post');

        $this->assertTrue($result instanceof Collection);
        $this->assertCount(2, $result);
        $this->assertEquals($first_post->name, $result[0]->name);
        $this->assertEquals($second_post->name, $result[1]->name);

        //Search by part of name with pagination
        $result = $this->repo->searchByName('post', $posts_per_page);

        $this->assertTrue($result instanceof LengthAwarePaginator);
        $this->assertCount(2, $result);
        $this->assertEquals($first_post->name, $result[0]->name);
        $this->assertEquals($second_post->name, $result[1]->name);
    }


}