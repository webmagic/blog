<?php

namespace Tests\Unit\Post;

use Webmagic\Blog\Posts\PostRepo as BaseRepo;

class PostRepo extends BaseRepo
{
    public function setEntity($entity_name)
    {
        $this->entity = $entity_name;
    }
}