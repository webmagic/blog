<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Blog common translation file
    |--------------------------------------------------------------------------
    |
    */

    'posts' => 'Посты',
    'add' => 'Добавить',
    'id' => 'ID',
    'slug' => 'Часть url',
    'title' => 'Заголовок',
    'category' => 'Категория',
    'categories' => 'Категории',
    'delete' => 'Удалить',
    'post-edition' => 'Рекдактирование поста',
    'post-creation' => 'Создание поста',
    'save-btn' => 'Сохранить',
    'meta-title' => 'Meta title',
    'meta-description' => 'Meta description',
    'meta-keywords' => 'Meta keywords',
    'main-image' => 'Основное изображение',
    'date' => 'Дата',
    'time' => 'Время',
    'description' => 'Краткое описание',
    'content' => 'Содержание',
    'gallery' => 'Галерея',
    'category-creation' => 'Создание категории',
    'category-edition' => 'Редактирование категории',
    'name' => 'Название',
    'image' => 'Изображение',
    'blog' => 'Блог'

];
