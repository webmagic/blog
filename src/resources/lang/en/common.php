<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Blog common translation file
    |--------------------------------------------------------------------------
    |
    */

    'posts' => 'Posts',
    'add' => 'Add',
    'id' => 'ID',
    'slug' => 'Slug',
    'title' => 'Title',
    'category' => 'Category',
    'categories' => 'Categories',
    'delete' => 'Delete',
    'post-edition' => 'Post edition',
    'post-creation' => 'Post creation',
    'save-btn' => 'Save',
    'meta-title' => 'Meta title',
    'meta-description' => 'Meta description',
    'meta-keywords' => 'Meta keywords',
    'main-image' => 'Main image',
    'date' => 'Date',
    'time' => 'Time',
    'description' => 'Short description',
    'content' => 'Content',
    'gallery' => 'Gallery',
    'category-creation' => 'Category creation',
    'category-edition' => 'Category edition',
    'name' => 'Name',
    'image' => 'Image',
    'blog' => 'Blog',

];
