<h1>@lang('blog::common.category-creation')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($category, ['url' => route('blog_module::category::store'), 'class' => 'js-submit', 'method' => 'POST']) !!}
                    @include('blog::categories._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>
