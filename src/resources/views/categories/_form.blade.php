{!! $component_generator->text('name', null, __('blog::common.name')) !!}
{!! $component_generator->text('title', null, __('blog::common.title')) !!}
{!! $component_generator->text('slug', null, __('blog::common.slug')) !!}

{!! $component_generator->imageLoad('img', __('blog::common.image'), isset($category['img']) ? $category->present()->mainImage : '') !!}

{!! $component_generator->text('meta_title', null, __('blog::common.meta-title')) !!}
{!! $component_generator->text('meta_description', null, __('blog::common.meta-description')) !!}
{!! $component_generator->text('meta_keywords', null, __('blog::common.meta-keywords')) !!}

<div class="box-footer">
    <button type="submit" class="btn btn-primary">@lang('blog::common.save-btn')</button>
</div>
