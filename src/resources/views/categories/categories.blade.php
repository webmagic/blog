<h1>@lang('blog::common.categories')</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="box">

                <div class="box-header">
                    <a href="{{route('blog_module::category::create')}}" class="btn btn-sm btn-success pull-right"><i class="fa fa-plus"> </i> @lang('blog::common.add')</a>
                </div>
                <div class="box-body">
                    <table class="js_data_table table table-bordered table-hover dataTable"
                           role="grid" aria-describedby="example2_info" data-searching="false"
                    data-sorting="false">
                        <thead>
                        <tr role="row">
                            <th>@lang('blog::common.id')</th>
                            <th>@lang('blog::common.name')</th>
                            <th class="text-center" rowspan="1" colspan="1" aria-label=""></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($categories as $category)
                            <tr role="row" class="js_category_{{$category['id']}} even posts-row ">
                                <td class="sorting_1">{!! $category['id'] !!}</td>
                                <td><a href="{{route('blog_module::category::edit', $category['id'])}}">{!! $category['name'] !!}</a></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{route('blog_module::category::edit', $category['id'])}}"
                                           class="category-edit btn btn-sm btn-info"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <button type="button" class="btn btn-sm btn-info dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a data-item=".js_category_{{$category['id']}}"
                                                   data-method="DELETE"
                                                   data-request="{{route('blog_module::category::delete', $category['id'])}}"
                                                   class="js_delete category-delete">@lang('blog::common.delete')
                                                </a>

                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
