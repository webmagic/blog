<h1>@lang('blog::common.category-edition')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($category, ['url' => route('blog_module::category::update',$category['id']), 'class' => 'js-submit', 'method' => 'PUT']) !!}
                    @include('blog::categories._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    @lang('blog::common.posts')
                    <a href="{{route('blog_module::blog::create')}}" class="btn btn-sm btn-success pull-right products-btn"><i class="fa  fa-plus"> </i> @lang('blog::common.add')</a>
                </div>
                <div class="box-body">
                    <table class="js_data_table table table-bordered table-hover dataTable"
                           role="grid"
                           aria-describedby="example2_info"
                           data-url="{{route('blog_module::blog::position')}}"
                            data-sorting="false">
                        <thead>
                        <tr role="row">
                            <th></th>
                            <th>@lang('blog::common.id')</th>
                            <th>@lang('blog::common.title')</th>
                            <th>@lang('blog::common.slug')</th>
                            @if(config('webmagic.dashboard.blog.category_use'))
                                <th>@lang('blog::common.category')</th>
                            @endif
                            <th class="text-center"></th>
                        </tr>
                        </thead>
                        <tbody class="js-sortable-with-handler" style="overflow-y: hidden;">
                        @foreach ($posts as $post)
                            <tr role="row" id="{{$post['id']}}"
                                class="even products-row js_product_{{$post['id']}} js-sortable-i">
                                <td class="text-center text-light-blue js-sortable-handler"
                                    style="cursor: move"><i class="fa  fa-arrows-v"></i></td>
                                <td class="sorting_1">{!! $post['id'] !!}</td>
                                <td>
                                    <a href="{{route('blog_module::blog::edit', $post['id'])}}" >{!! $post['name'] !!}</a>
                                </td>

                                <td>{!! $post['slug'] !!}</td>


                                @if (config('webmagic.dashboard.blog.category_use') )
                                    <td>
                                        <a href="{{route('blog_module::category::edit', $post['category_id'])}}" >
                                            @if(!empty($category) && isset($category['id']))
                                                {{$category['name']}}
                                            @else {{''}}
                                            @endif
                                        </a>
                                    </td>
                                @endif
                                <td class="text-center" style="min-width: 80px;">
                                    <div class="btn-group">
                                        <a href="{{route('blog_module::blog::edit', $post['id'])}}"
                                           class="products-edit btn btn-xs btn-info"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <button type="button" class="btn btn-xs btn-info dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a data-item=".js_product_{{$post['id']}}"
                                                   data-method="DELETE"
                                                   data-request="{{route('blog_module::blog::delete', $post['id'])}}"
                                                   class="js_delete products-delete">@lang('blog::common.delete')
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
