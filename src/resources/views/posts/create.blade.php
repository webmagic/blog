<h1>@lang('blog::common.post-creation')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($post, ['url' => route('blog_module::blog::store'), 'class' => 'js-submit', 'method' => 'POST']) !!}
                    @include('blog::posts._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>

