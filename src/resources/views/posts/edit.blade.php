<h1>@lang('blog::common.post-edition')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($post, ['url' => route('blog_module::blog::update',$post['id']), 'class' => 'js-submit', 'method' => 'PUT']) !!}
                    @include('blog::posts._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>
