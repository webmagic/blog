<div class="row">
    <div class="col-lg-4 col-md-6 col-xs-12">
        {!! $component_generator->imageLoad('main_image', __('blog::common.main-image'), isset($post['main_image']) ? $post->present()->mainImage : '') !!}
    </div>

    <div class="col-lg-8 col-md-6 col-xs-12">
        {!! $component_generator->text('name', null, __('blog::common.title')) !!}
        {!! $component_generator->text('slug', null, __('blog::common.slug')) !!}
        <div class="js-copy-item form-horizontal row form-group">
            <div class="col-md-3">
                <label class="control-label">@lang('blog::common.date')</label>
                {!! $form_builder->date('date', isset($post['‘date’']) ? $post['‘date’']: null, ['class'=>'form-control']) !!}
            </div>
            <div class="col-md-3">
                <label class="control-label">@lang('blog::common.time')</label>
                {!! $form_builder->time('time', isset($post['‘time’']) ? $post['‘time’']: null, ['class'=>'form-control']) !!}
            </div>
        </div>
    </div>
</div>

@if(config('webmagic.dashboard.blog.category_use'))
    {!! $component_generator->select('category_id', $categories, null, __('blog::common.category')) !!}
@endif
{!! $component_generator->textareaWYSIHTML5('description', null, __('blog::common.description')) !!}
{!! $component_generator->textareaWYSIHTML5('content', null, __('blog::common.content')) !!}

{!! $component_generator->imageLoad('images', __('blog::common.gallery'), isset($post['images']) ? $post->present()->gallery : [], ['multiple' => '']) !!}

{!! $component_generator->text('meta_title', null, __('blog::common.meta-title')) !!}
{!! $component_generator->text('meta_description', null, __('blog::common.meta-description')) !!}
{!! $component_generator->text('meta_keywords', null, __('blog::common.meta-keywords')) !!}

<div class="box-footer">
    <button type="submit" class="btn btn-primary">@lang('blog::common.save-btn')</button>
</div>
