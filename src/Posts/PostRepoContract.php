<?php

namespace Webmagic\Blog\Posts;


use Webmagic\Core\Entity\EntityRepoInterface;

interface PostRepoContract extends EntityRepoInterface
{

    /**
     * Create new post
     *
     * @param array $post_data
     * @return Model
     */
    public function create(array $post_data);

    /**
     * Update post data
     *
     * @param int|string $post_id
     * @param array $post_data
     * @return mixed
     */
    public function update($post_id, array $post_data);

    /**
     * Get all posts and group by category if true
     *
     * @param null $products_per_page
     * @return \Illuminate\Contracts\Pagination\Paginator|Collection|\Illuminate\Pagination\Paginator
     */
    public function getAll($products_per_page = null);

    /**
     * Get post by ID with category if true
     *
     * @param int|string $post_id
     * @return Model|null
     */
    public function getByID($post_id);

    /**
     * Return post by slug id
     *
     * @param $post_slug
     * @return Model|null
     */
    public function getBySlug($post_slug);

    /**
     * Get posts by categories and paginate
     *
     * @param $category_id
     * @return array|static[]
     * @internal param null $posts_per_page
     * @internal param $paginate
     */
    public function getByCategoryID($category_id);

    /**
     * Change post position
     *
     * @param $moving_entity_id
     * @param $base_entity_id
     */
    public function moveAfter($moving_entity_id, $base_entity_id);

    /**
     * Change product position
     *
     * @param $moving_entity_id
     * @param $base_entity_id
     */
    public function moveBefore($moving_entity_id, $base_entity_id);

    /**
     * Search by part o name
     *
     * @param null $post_name_part
     * @param null $post_per_page
     * @return mixed
     */
    public function searchByName($post_name_part = null, $post_per_page = null);

    /**
     * Return posts sort by created in specified amount plus sorting
     *
     * @param $amt_posts
     * @param string $sorting_type
     * @return mixed
     */
    public function sortByCreated($amt_posts, $sorting_type = 'desc');




}