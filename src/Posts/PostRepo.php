<?php

namespace Webmagic\Blog\Posts;

use Webmagic\Core\Entity\EntityRepo;
use IvanLemeshev\Laravel5CyrillicSlug\Slug as Slug;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class PostRepo extends EntityRepo implements PostRepoContract
{
    protected $entity = Post::class;

    /**
     * Return next post in same category based on position
     *
     * @param Post $post
     *
     * @return Model|null|PostRepo
     * @throws \Exception
     */
    public function getNext(Post $post)
    {
        $query = $this->query();
        $query->where('position', '>', $post->position)
            ->where('category_id', $post->category_id);

        return $this->realGetOne($query);
    }

    /**
     * Return previous post in same category based on position
     *
     * @param Post $post
     *
     * @return Model|null|PostRepo
     * @throws \Exception
     */
    public function getPrevious(Post $post)
    {
        $query = $this->query();
        $query->where('position', '<', $post->position)
            ->where('category_id', $post->category_id);

        return $this->realGetOne($query);
    }

    /**
     * Generate slug
     *
     * @param $text
     *
     * @return mixed
     */
    protected function slugGenerator($text)
    {
        $slugService = new Slug();
        $slug = $slugService->make($text, '-');

        return $slug;
    }


    /**
     * Get one entity based on query
     *
     * @param Builder|null $post_query
     *
     * @return Model|null|static
     */
    protected function realGetOne(Builder $post_query = null)
    {
        if (is_null($post_query)) {
            $post_query = $this->newQuery();
        }

        $post_query->with('category');

        return $post_query->first();
    }

    /**
     * Get many entities based on query
     *
     * @param Builder|null $post_query
     * @param null         $products_per_page
     *
     * @return $this|Collection|static[]
     */
    protected function realGetMany(Builder $post_query = null, $products_per_page = null)
    {
        if (is_null($post_query)) {

            $post_query = $this->newQuery();
        }

        $post_query = $post_query->orderBy('blog_posts.position');

        //Paginate
        if (!is_null($products_per_page)) {
            $products = $post_query->paginate($products_per_page);

            //Need for correct links generation when paginate
            return $products->appends(Input::except('page'));

        }

        return $post_query->get();
    }

    /**
     * Create new post
     *
     * @param array $post_data
     *
     * @return Model
     */
    public function create(array $post_data)
    {
        //Generate slug if not exist
        $post_data['slug'] = $this->slugGenerator($post_data['slug']);

        if ($post_data['slug'] == '') {
            $post_data['slug'] = $this->slugGenerator($post_data['name']);
        }

        return parent::create($post_data);
    }

    /**
     * Update post data
     *
     * @param int|string $post_id
     * @param array      $post_data
     *
     * @return mixed
     */
    public function update($post_id, array $post_data)
    {
        //Generate slug if not exist
        $post_data['slug'] = $this->slugGenerator($post_data['slug']);

        if ($post_data['slug'] == '') {
            $post_data['slug'] = $this->slugGenerator($post_data['name']);
        }

        return parent::update($post_id, $post_data);
    }

    /**
     * Get all posts and group by category if true
     *
     * @param null $products_per_page
     *
     * @return \Illuminate\Contracts\Pagination\Paginator|Collection|\Illuminate\Pagination\Paginator
     * @throws \Exception
     */
    public function getAll($products_per_page = null)
    {
        $query = $this->query();

        return $this->realGetMany($query, $products_per_page);
    }

    /**
     * Get post by ID
     *
     * @param int|string $post_id
     *
     * @return Model|null
     * @throws \Exception
     */
    public function getByID($post_id)
    {
        $query = $this->query();
        $query->where('id', $post_id);

        return $this->realGetOne($query);
    }

    /**
     * Get post by ID's
     *
     * @param int|string $post_id
     *
     * @return Model|null
     */
    public function getByIds($post_id)
    {
        $query = $this->query();
        $query->whereIn('id', $post_id);

        return $this->realGetMany($query);
    }

    /**
     * Return post by slug
     *
     * @param $post_slug
     *
     * @return Model|null
     */
    public function getBySlug($post_slug)
    {
        $query = $this->query();
        $query->where('slug', $post_slug);

        return $this->realGetOne($query);
    }


    /**
     * Get posts by categories and paginate
     *
     * @param      $category_id
     * @param null $products_per_page
     *
     * @return Collection|PostRepo|static[]
     */
    public function getByCategoryID($category_id, $products_per_page = null)
    {
        $query = $this->query();
        $query->where('category_id', $category_id);

        return $this->realGetMany($query, $products_per_page);
    }

    /**
     * Change post position
     *
     * @param $moving_entity_id
     * @param $base_entity_id
     */
    public function moveAfter($moving_entity_id, $base_entity_id)
    {
        $query = $this->query()->find($moving_entity_id);
        $query->moveAfter($this->query()->find($base_entity_id));
    }

    /**
     * Change product position
     *
     * @param $moving_entity_id
     * @param $base_entity_id
     */
    public function moveBefore($moving_entity_id, $base_entity_id)
    {
        $query = $this->query()->find($moving_entity_id);
        $query->moveBefore($this->query()->find($base_entity_id));
    }

    /**
     * Search by part o name
     *
     * @param null $post_name_part
     * @param null $post_per_page
     *
     * @return mixed
     */
    public function searchByName($post_name_part = null, $post_per_page = null)
    {
        $query = $this->query();

        $query = $query->search($post_name_part, null, false, true);

        return $this->realGetMany($query, $post_per_page);
    }

    /**
     * Return posts sort by created in specified amount plus sorting
     *
     * @param        $amt_posts
     * @param string $sorting_type
     *
     * @return mixed
     */
    public function sortByCreated($amt_posts, $sorting_type = 'desc')
    {
        $query = $this->query();
        $query->orderBy("created_at", $sorting_type)->take($amt_posts);

        return $this->realGetMany($query);
    }
}