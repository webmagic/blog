<?php

namespace Webmagic\Blog\Posts;


use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
use Webmagic\Core\Presenter\PresentableTrait;
use Webmagic\Core\Presenter\Presenter;

class Post extends Model
{

    use PresentableTrait, SortableTrait, SearchableTrait;

    /** @var  Presenter class that using for present model */
    protected $presenter = PostPresenter::class;


    /** @var string Entity table */
    protected $table = 'blog_posts';
    

    /**
     *  The attributes that are mass assignable.
     */
    protected $fillable = ['name', 'category_id', 'description', 'main_image', 'images', 'tag', 'slug', 'content','meta_title', 'meta_description', 'meta_keywords', 'created_at', 'updated_at'];


    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable;


    /**
     * Model constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, config('webmagic.blog.posts_available_fields'));
        $this->searchable = array_merge($this->fillable, config('webmagic.blog.post_searchable'));
        parent::__construct($attributes);
    }

    /**
     * Categories relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->belongsTo('Webmagic\Blog\Categories\Category');
    }
}