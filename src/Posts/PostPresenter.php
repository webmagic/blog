<?php


namespace Webmagic\Blog\Posts;

use Webmagic\Core\Presenter\Presenter as CorePresenter;

class PostPresenter extends CorePresenter
{

    /**
     * Main image
     *
     * @return string
     */
    public function mainImage()
    {
        return $this->prepareImageURL($this->main_image);
    }


    /**
     * Return russian month
     *
     * @return string
     */
    public function rusMonth()
    {
        return $this->russianMonth($this->created_at);
    }

    /**
     * Prepare URLs for gallery images
     *
     * @return array
     */
    public function gallery()
    {
        if(!isset($this->gallery)) {
            if ($product_images = explode('|', $this->images)) {
                foreach ($product_images as &$image) {
                    if (strlen($image) > 0) {
                        $image = $this->prepareImageURL($image);
                    } else {
                        $image = '';
                    }
                }

                $this->gallery = strlen($product_images[0]) > 0 ? $product_images : [];
            }
        }

        return $this->gallery;
    }


    /**
     * Prepare months for russian language
     *
     * @param $date
     * @return string
     */
    public function russianMonth($date){

        $date=explode(".", $date->format("d.m.Y"));

        switch ($date[1]){
            case 1: $month='ЯНВ'; break;
            case 2: $month='ФЕВ'; break;
            case 3: $month='МАР'; break;
            case 4: $month='АПР'; break;
            case 5: $month='МАЯ'; break;
            case 6: $month='ИЮН'; break;
            case 7: $month='ИЮЛ'; break;
            case 8: $month='АВГ'; break;
            case 9: $month='СЕН'; break;
            case 10: $month='ОКТ'; break;
            case 11: $month='НОЯ'; break;
            case 12: $month='ДЕК'; break;
        }

        return $month;
    }

    /**
     * Prepare url for images
     *
     * @param $file_name
     *
     * @return string
     */
    protected function prepareImageURL($file_name)
    {
        return asset(config('webmagic.blog.posts_img_path') . '/' . $file_name);
    }

}