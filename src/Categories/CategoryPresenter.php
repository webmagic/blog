<?php


namespace Webmagic\Blog\Categories;

use Webmagic\Core\Presenter\Presenter as CorePresenter;

class CategoryPresenter extends CorePresenter
{

    /**
     * Main image
     *
     * @return string
     */
    public function mainImage()
    {
        return $this->prepareImageURL($this->img);
    }


    /**
     * Prepare url for images
     *
     * @param $file_name
     *
     * @return string
     */
    protected function prepareImageURL($file_name)
    {
        return asset(config('webmagic.blog.categories_img_path') . '/' . $file_name);
    }

}