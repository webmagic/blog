<?php

namespace Webmagic\Blog\Categories;

use Webmagic\Core\Entity\EntityRepo;
use IvanLemeshev\Laravel5CyrillicSlug\Slug as Slug;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class CategoryRepo extends EntityRepo implements CategoryRepoContract
{

    protected $entity = Category::class;

    /**
     * Generate slug
     *
     * @param $text
     * @return mixed
     */
    protected function slugGenerator($text)
    {
        $slugService = new Slug();
        $slug = $slugService->make($text,'-');

        return $slug;
    }


    /**
     * Get one entity based on query
     *
     * @param Builder $query
     *
     * @return null|Model
     */
    protected function realGetOne(Builder $query)
    {
        $query->with('posts');

        return $query->first();
    }

    /**
     *  Get all entities based on query
     *
     * @param Builder $query
     *
     * @return Collection|Paginator
     */
    protected function realGetMany(Builder $query)
    {
        $query->with('posts');

        return $query->get();
    }


    /**
    * Create new category
    *
    * @param $category_data
    *
    * @return static
    */
    public function create(array $category_data)
    {
        $category_data['slug'] = $this->slugGenerator($category_data['slug']);

        if($category_data['slug'] == ''){
            $category_data['slug'] = $this->slugGenerator($category_data['name']);
        }

        return parent::create($category_data);
    }


    /**
     * Update category data
     *
     * @param $category_id
     * @param $category_data
     *
     * @return mixed
     */
    public function update($category_id, array $category_data)
    {
        $category_data['slug'] = $this->slugGenerator($category_data['slug']);

        if($category_data['slug'] == ''){
            $category_data['slug'] = $this->slugGenerator($category_data['name']);
        }

        return parent::update($category_id, $category_data);
    }


    /**
     * Get category by ID with posts if true
     *
     * @param int|string $category_id
     *
     * @return \Illuminate\Database\Eloquent\Model|null|Model
     */
    public function getByID($category_id)
    {
        $query = $this->query();
        $query->where('id', $category_id);

        return $this->realGetOne($query);
    }


    /**
     * Get all categories with posts if true
     *
     * @return Collection
     */
    public function getAll()
    {
        $query = $this->query();

        return $this->realGetMany($query);
    }

    /**
     * Get all active categories with posts if true
     *
     * @return Collection
     */
    public function getAllActive():Collection
    {
        $query = $this->query();
        $query->where('active', true);

        return $this->realGetMany($query);
    }

    /**
     * Get category by slug
     *
     * @param $slug
     *
     * @return Collection|Paginator
     */
    public function getBySlug($slug)
    {
        $query = $this->query();
        $query->where('slug', $slug);

        return $this->realGetOne($query);
    }


}