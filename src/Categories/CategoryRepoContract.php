<?php

namespace Webmagic\Blog\Categories;

use Webmagic\Core\Entity\EntityRepoInterface;
use Illuminate\Database\Eloquent\Collection;

interface CategoryRepoContract extends EntityRepoInterface
{

    /**
     * Create new category
     *
     * @param $category_data
     *
     * @return static
     */
    public function create(array $category_data);

    /**
     * Update category data
     *
     * @param $category_id
     * @param $category_data
     *
     * @return mixed
     */
    public function update($category_id, array $category_data);

    /**
     * Get category by ID with posts if true
     *
     * @param int|string $category_id
     * @return \Illuminate\Database\Eloquent\Model|null|Model
     */
    public function getByID($category_id);

    /**
     * Get all categories with posts if true
     *
     * @return Collection
     */
    public function getAll();

    /**
     * Get all active categories with posts if true
     *
     * @return Collection
     * @internal param bool $with_posts
     */
    public function getAllActive(): Collection;


}