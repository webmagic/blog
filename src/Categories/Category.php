<?php


namespace Webmagic\Blog\Categories;


use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;
use Webmagic\Core\Presenter\PresentableTrait;
use Webmagic\Core\Presenter\Presenter;


class Category extends Model
{
    use PresentableTrait, SortableTrait;

    /** @var  Presenter class that using for present model */
    protected $presenter = CategoryPresenter::class;


    /** @var string Entity table */
    protected $table = 'blog_categories';

    /**
     * Hide params
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     *  The attributes that are mass assignable.
     */
    protected $fillable = ['name', 'slug', 'title','img','meta_title', 'meta_description', 'meta_keywords', 'active'];


    /**
     * Model constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, config('webmagic.blog.category_available_fields'));

        parent::__construct($attributes);
    }

    /**
     * Many posts relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('Webmagic\Blog\Posts\Post');
    }
}