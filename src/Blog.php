<?php


namespace Webmagic\Blog;

use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Webmagic\Blog\Categories\CategoryRepoContract;
use Webmagic\Blog\Posts\PostRepoContract;


/**********************************************************************************************************************
* Webmagic\Category\CategoryRepoContract
**********************************************************************************************************************
* @method void categoryCreate(array $category_data)
* @method void categoryUpdate(string $entity_id, array $category_data)
* @method Collection categoryGetAll(string $with_posts)
* @method Collection categoryGetAllActive(string $with_posts)
* @method void categoryGetByID(string $id, string $with_posts)
* @method void categoryGetBySlug(string $slug, string $with_posts)
* @method void categoryDestroy(string $entity_id)
* @method void categoryDestroyAll()
* @method array categoryGetForSelect(string $value, string $key)
*
**********************************************************************************************************************

**********************************************************************************************************************
* Webmagic\Post\PostRepoContract
**********************************************************************************************************************
*
* @method void postSearchByName(string $post_name_part, string $post_per_page)
* @method void postSortByCreated(string $amt_posts, string $sorting_type)
* @method void postGetBySlug(string $post_slug, string $with_category)
* @method void postGetByID(number $id)
* @method void postGetByIds(array|number $id)
* @method void postGetAll(string $posts_per_page)
* @method void postGetByCategoryID(string $category_id, string $with_category)
* @method void postMoveAfter(string $moving_entity_id, string $base_entity_id)
* @method void postMoveBefore(string $moving_entity_id, string $base_entity_id)
* @method void postCreate(array $post_data)
* @method void postUpdate(string $post_id, array $post_data)
* @method void postDestroy(string $entity_id)
* @method void postDestroyAll()
* @method array postGetForSelect(string $value, string $key)
*
**********************************************************************************************************************

**********************************************************************************************************************/
class Blog
{
    /** @var array Abstract entities for which access provided  */
    protected $classes = [
        'post' => PostRepoContract::class,
        'category' => CategoryRepoContract::class,

    ];

    /**
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        foreach ($this->classes as $alias => $abstract){
            if(strpos($method, $alias) === 0){
                $obj = app()->make($this->classes[$alias]);
                return $this->callMethod($obj, $args, $method, $alias);
            }
        }

        return new MethodNotAllowedException($method);
    }

    /**
     * Call method on obj with method name cleaning
     *
     * @param $obj
     * @param $args
     * @param $fullMethodName
     * @param $removeFromMethod
     *
     * @return mixed
     */
    protected function callMethod($obj, $args, $fullMethodName, $removeFromMethod)
    {
        $method = str_replace($removeFromMethod, '', $fullMethodName);

        try{
            return call_user_func_array([$obj, $method], $args);
        } catch (MethodNotAllowedException $e){
            return $e;
        }
    }



}