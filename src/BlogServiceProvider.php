<?php

namespace Webmagic\Blog;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Webmagic\Blog\Categories\CategoryRepo;
use Webmagic\Blog\Categories\CategoryRepoContract;
use Faker\Generator as FakerGenerator;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Webmagic\Blog\Posts\PostRepo;
use Webmagic\Blog\Posts\PostRepoContract;

class BlogServiceProvider extends ServiceProvider
{
    /**
     * Register blog service
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/blog.php', 'webmagic.blog'
        );

        $this->registerServices();
    }

    /**
     * Boot posts service
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        $this->registerPublishes();
        $this->registerMiddleware($router);
        $this->registerFactories();
        $this->loadMigrations();

    }

    /**
     * Register all needs publishes
     */
    protected function registerPublishes()
    {
        //Migrations publishing
        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('/migrations'),
        ], 'migrations');

        //Seeds publishing
        $this->publishes([
            __DIR__.'/database/seeds/' => database_path('/seeds'),
        ], 'seeds');

        //Config publishing
        $this->publishes([
            __DIR__.'/config/blog.php' => config_path('webmagic/blog.php'),
        ], 'config');

    }

    /**
     * Load migrations
     */
    protected function loadMigrations()
    {
        $this->loadMIgrationsFrom( __DIR__.'/database/migrations/');
    }

    /**
     * Registering blog services
     */
    protected function registerServices()
    {
        $this->app->bind(CategoryRepoContract::class, CategoryRepo::class);
        $this->app->bind(PostRepoContract::class, PostRepo::class);
    }
    /**
     * Middleware registration
     *
     * @param $router
     */
    protected function registerMiddleware($router)
    {
        $router->middlewareGroup('blog', [
            \Illuminate\Cookie\Middleware\EncryptCookies::class,
            \Illuminate\Session\Middleware\StartSession::class,
        ]);
    }

    /**
     * Factories registration
     */
    protected function registerFactories()
    {
        $this->app->singleton(EloquentFactory::class, function ($app){
            return EloquentFactory::construct($app->make(FakerGenerator::class),  __DIR__.'/database/factories/');
        });
    }


}