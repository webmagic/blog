<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Image storing paths
    |--------------------------------------------------------------------------
    |
    | In this paths will saving all images for blog module
    |
    */

    'posts_img_path' => 'webmagic/blog/img/posts',
    'categories_img_path' => 'webmagic/blog/img/categories',


    /*
    |--------------------------------------------------------------------------
    | Fields available in model additional to base fields
    |--------------------------------------------------------------------------
    |
    | If needs add additional field add it in migration
    | and add there for activate saving data
    |
    */

    'posts_available_fields' => [],
    'category_available_fields' => [],


    /*
    |--------------------------------------------------------------------------
    | Searchable
    |--------------------------------------------------------------------------
    |
    | Columns and their priority in search results.
    | Columns with higher values are more important.
    | Columns with equal values have equal importance.
    |
    */

    'post_searchable' => [
        'columns' => [
            'blog_posts.name' => 10,
        ],
    ]

];
