<?php
/*
   |--------------------------------------------------------------------------
   | Module Request. Dashboard settings
   |--------------------------------------------------------------------------
   |
   | This setting needs only if you want use module Dashboard
   |
   */

return [
    /*
    |--------------------------------------------------------------------------
    | Use or not use categories
    |--------------------------------------------------------------------------
    */

    'category_use' => $category_use = true,

    /*
    |--------------------------------------------------------------------------
    | Use or not use hash and pagination
    |--------------------------------------------------------------------------
    |
    | Add hash for images
    |
    */
    'hash_use' => $hash_use = false,
    'pagination_use' => $pagination_use = false,
    'pagination_num' => $pagination_num = 10,

    /*
    |--------------------------------------------------------------------------
    | Module routes_dashboard prefix
    |--------------------------------------------------------------------------
    |
    | This prefix use for generation all routes for module
    |
    */

    'prefix' => 'dashboard/blog',

    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Can use middleware for access to module page
    |
    */

    'middleware' => ['blog'],

    /*
    |--------------------------------------------------------------------------
    | Parent category in dashboard
    |--------------------------------------------------------------------------
    |
    | Use for generation module page in dashboard.
    | Use '' if not need parent category
    |
    */

    'menu_parent_category' => '',

    /*
    |--------------------------------------------------------------------------
    | Dashboard menu item config
    |--------------------------------------------------------------------------
    |
    | Config new item in dashboard menu
    |
    */

    'menu_item_name' => 'blog',

    'dashboard_menu_item' => [
        'link' => 'dashboard/blog/post',
        'text' => 'blog::common.blog',
        'icon' => 'fa-newspaper-o',
        'subitems' => $category_use ?
            [
                 [
                    'link' => 'dashboard/blog/post',
                    'text' => 'blog::common.posts',
                    'icon' => 'fa-file-text-o',
                     'active_rules' => [
                         'routes_parts'=>[
                             'blog_module::blog::'
                         ]
                     ]
                ],
                "category" => [
                    'link' => 'dashboard/blog/category',
                    'text' => 'blog::common.categories',
                    'icon' => 'fa-bookmark-o',
                    'active_rules' => [
                        'routes_parts'=>[
                            'blog_module::category::'
                        ]
                    ]
                ]
            ] : [],


    ]
];
