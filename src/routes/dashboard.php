<?php

Route::group([
    'prefix' => config('webmagic.dashboard.blog.prefix'),
    'namespace' => '\Webmagic\Blog\Http\Controllers',
    'middleware' => config('webmagic.dashboard.blog.middleware'),
    'as' => 'blog_module::'
],
        function () {

            /* Post Control*/
            Route::group([
                'as' => 'blog::',
                'prefix'=> 'post'],
                function() {

                    Route::get('/', [
                        'as' => 'index',
                        'uses' => 'PostDashboardController@posts']
                    );

                    Route::get('/create', [
                        'as' => 'create',
                        'uses' => 'PostDashboardController@create']
                    );

                    Route::post('/', [
                        'as' => 'store',
                        'uses' => 'PostDashboardController@store']
                    );

                    Route::get('{id}/edit', [
                        'as' => 'edit',
                        'uses' => 'PostDashboardController@edit']
                    );

                    Route::put('post/{id}', [
                        'as' => 'update',
                        'uses' => 'PostDashboardController@update']
                    );

                    Route::delete('post/{id}', [
                        'as' => 'delete',
                        'uses' => 'PostDashboardController@destroy']
                    );

                    Route::post('post/position/update', [
                        'as' => 'position',
                        'uses' => 'PostDashboardController@positionUpdate']
                    );
                }
            );

            if(config('webmagic.dashboard.blog.category_use')) {

            /* Category control*/

                Route::group([
                    'as' => 'category::',
                    'prefix' => 'category'
                ],
                    function () {

                        Route::get('/', [
                            'as' => 'index',
                            'uses' => 'CategoryDashboardController@index']
                        );

                        Route::get('create', [
                            'as' => 'create',
                            'uses' => 'CategoryDashboardController@create']
                        );

                        Route::post('/', [
                            'as' => 'store',
                            'uses' => 'CategoryDashboardController@store']
                        );

                        Route::get('{id}/edit', [
                            'as' => 'edit',
                            'uses' => 'CategoryDashboardController@edit']
                        );

                        Route::put('{id}', [
                            'as' => 'update',
                            'uses' => 'CategoryDashboardController@update']
                        );

                        Route::delete('{id}', [
                            'as' => 'delete',
                            'uses' => 'CategoryDashboardController@destroy']
                        );
                    }
                );
            }
        }
);