<?php

namespace Webmagic\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;


class BlogController extends Controller
{

   /**
     * Move images and prepare URLs for DB saving
     *
     * @param Request $request
     * @param bool $field_name
     * @param $path
     * @return Request
     */
    protected function imagesPrepare(Request $request, $field_name, $path)
    {
        // check if
        // request has mark for updated needed field
        // and value of first item was set
        $field_update_key = $field_name.'_update';
        if($request->has($field_update_key) && $request[$field_update_key] === 'true' && $request->has($field_name.'0')){

            $file_key = 0;
            $file_name = $field_name.$file_key;
            $names_compilation = '';

            //save all items of field name
            while($request->file($file_name)){

                //add uniq ID if functionality on in config
                $real_file_name = $request[$file_name]->getClientOriginalName();
                $real_file_name = config('webmagic.dashboard.blog.hash_use') ? uniqid() . $real_file_name : $real_file_name;
                $names_compilation .= $request[$file_name]->move(public_path($path), $real_file_name )->getFilename() . "|";

                $file_key++;
                $file_name = $field_name.$file_key;
            }

            $request[$field_name] = rtrim($names_compilation, '|');

        } else {
            //we should do nothing and remove key from request
            unset($request[$field_name]);
        }

        return $request;
    }
}