<?php

namespace Webmagic\Blog\Http\Controllers;

use Webmagic\Blog\Blog;
use Illuminate\Http\Request;
use Webmagic\Blog\Http\Requests\PostRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Carbon\Carbon;

class PostDashboardController extends BlogController
{
    use ValidatesRequests;

    /**
     * Show posts list
     *
     * @param Blog $blog
     *
     * @return Factory|\Illuminate\View\View
     */
    public function posts(Blog $blog)
    {
        $posts = $blog->postGetAll();
        if (config('webmagic.dashboard.blog.category_use')) {
            $categories = $blog->categoryGetForSelect('name', 'id');
        }

        $content = view('blog::posts.posts', compact('posts', 'categories'));

        return $this->getDashboardWithContent($content);
    }


    /**
     * Show page for post creating
     *
     * @param Blog $blog
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Blog $blog)
    {
        $post = '';
        $categories = $blog->categoryGetForSelect('name', 'id');

        $content = view('blog::posts.create', compact('post', 'categories'));

        return $this->getDashboardWithContent($content);
    }


    /**
     * Create post
     *
     * @param PostRequest $request
     * @param Blog $blog
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(PostRequest $request, Blog $blog)
    {
        $request = $this->imagesPrepare($request);

        $request_data = $request->all();

        if ($request->has('date') || $request->has('time')) {
            $time = Carbon::parse($request_data['date'].' '.$request_data['time']);
            $request_data['created_at'] = $time->format('Y-m-d H:i:s');
        }

        if(!$blog->postCreate($request_data)){
            return response('При создании записи возникли ошибки', 500);
        }
    }


    /**
     * Show form for creating post
     *
     * @param $post_id
     * @param Blog $blog
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     * @internal param BlogService $postService
     */
    public function edit($post_id, Blog $blog)
    {
        if(!($post = $blog->postGetByID($post_id))){
            return response('Запись не найдена', 404);
        }
        $categories = $blog->categoryGetForSelect('name', 'id');
        $post_date = explode(' ', $post['created_at']);
        $post['date'] = $post_date[0];
        $post['time'] = $post_date[1];

        $content = view('blog::posts.edit', compact( 'post', 'categories'));

        return $this->getDashboardWithContent($content);
    }


    /**
     * Update post by ID
     *
     * @param                $post_id
     * @param PostRequest $request
     * @param Blog $blog
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($post_id, PostRequest $request, Blog $blog)
    {
        $request = $this->imagesPrepare($request);

        $request_data = $request->all();

        if ($request->has('date') || $request->has('time')) {
            $time = Carbon::parse($request_data['date'].' '.$request_data['time']);
            $request_data['created_at'] = $time->format('Y-m-d H:i:s');
        }


        if(!$blog->postUpdate($post_id, $request_data)){
            return response('При обновлении записи возникли ошибки', 500);
        }
    }


    /**
     * Destroy post
     *
     * @param $post_id
     * @param Blog $blog
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($post_id, Blog $blog)
    {
        if(!($post = $blog->postDestroy($post_id))){
            return response('Запись не найдена', 404);
        }
    }


    /**
     * Update product position
     *
     * @param Request $request
     * @param Blog $blog
     */
    public function positionUpdate(Request $request, Blog $blog)
    {
        $this->validate($request, [
            'reference_type' => "required|in:before,after",
            'entity_id' => "required|exists:blog_posts,id",
            'reference_entity_id' => "required|exists:blog_posts,id"
        ]);

        //Change after to before because it specific js script work
        if($request['reference_type'] === 'after'){
            $blog->postMoveBefore($request['entity_id'], $request['reference_entity_id']);
        } else {
            $blog->postMoveAfter($request['entity_id'], $request['reference_entity_id']);
        }
    }


    /**
     * This function is used in order to not transmit a large number of parameter for the preparation and recording of images
     *
     * @param Request $request
     * @param string $field_name
     * @param string $path
     * @return Request
     */
    protected function imagesPrepare(Request $request, $field_name = '', $path = '')
    {
        $field_name = $field_name == '' ? 'main_image' : $field_name;
        $path = $path == '' ? config('webmagic.blog.posts_img_path') : $path;

        return parent::imagesPrepare($request, $field_name, $path); // TODO: Change the autogenerated stub
    }


    /**
     * Prepare Dashboard and set content inside
     *
     * @param $content
     * @return mixed
     */
    protected function getDashboardWithContent($content)
    {
        $dashboard = app()->make(\Webmagic\Dashboard\Dashboard::class);
        $dashboard->content($content);
        return $dashboard->render();
    }
}