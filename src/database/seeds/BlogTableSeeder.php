<?php

use Illuminate\Database\Seeder;
use Webmagic\Blog\Category\Category;
use Webmagic\Blog\Post\Post;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class, 50)->create()->each(function($category) {
            $category->posts()->save(factory(blog::class)->make());
        });

    }
}
