<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
use IvanLemeshev\Laravel5CyrillicSlug\Slug;

/**
 * Category
 */
$factory->define(\Webmagic\Blog\Categories\Category::class, function (Faker\Generator $faker) {
    $slug = new Slug();
    return [
        'name' => $name = $faker->word,
        'slug' => $slug->make($name),
        'title' => $faker->word,
        'active' => 1,
        'img' => $faker->imageUrl(640,480),
    ];
});

/**
 * Post
 */
$factory->define(\Webmagic\Blog\Posts\blog::class, function (Faker\Generator $faker) {
    $slug = new Slug();

    return [
        'name' => $name = $faker->word,
        'slug' => $slug->make($name),
        'main_image' => $faker->imageUrl(640,480),
        'images' => $faker->imageUrl(640,480),
        'description' => $faker->text(),
        'content' => $faker->text(),

    ];
});
