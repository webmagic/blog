<?php

namespace Webmagic\Blog;

use Illuminate\Routing\Router;
use Webmagic\Dashboard\Dashboard;

class BlogDashboardServiceProvider extends BlogServiceProvider
{
    /**
     * Register posts service
     */
    public function register()
    {
        parent::register();

        $this->mergeConfigFrom(
            __DIR__.'/config/blog_dashboard.php', 'webmagic.dashboard.blog'
        );
    }

    /**
     * Boot posts service
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        parent::boot( $router);

        //Routs registering
        include 'routes/dashboard.php';

        //Load Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'blog');

        //Config publishing
        $this->publishes([
            __DIR__.'/config/blog_dashboard.php' => config_path('webmagic/dashboard/blog.php'),
        ], 'config');

        //Views publishing
        $this->publishes([
            __DIR__.'/resources/views/' => base_path('resources/views/vendor/blog/'),
        ], 'views');

        //Add items menu
        $this->includeMenuForDashboard();

        $this->loadTranslations();
    }

    /**
     * Including menu items for new dashboard
     */
    protected function includeMenuForDashboard()
    {
        $menu_item_config = config('webmagic.dashboard.blog.dashboard_menu_item');

        if(!$menu_item_config) {
            return false;
        }

        $dashboard = app()->make(Dashboard::class);
        $dashboard->getMainMenu()->addMenuItems($menu_item_config);
    }

    /**
     * Load translation
     */
    protected function loadTranslations()
    {
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'blog');
    }

}